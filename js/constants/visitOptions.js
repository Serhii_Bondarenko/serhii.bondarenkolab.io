import Visit from "../entities/visit/visit.js";
import CardiologistVisit from "../entities/visit/cardiologistVisit.js";
import DentistVisit from "../entities/visit/dentistVisit.js";
import TherapistVisit from "../entities/visit/therapistVisit.js";
import {inputClasses} from "./input.js";


const today = new Date();
const yyyy = today.getFullYear();
const mm = today.getMonth() + 1;
const dd = today.getDate();
const currDate = `${yyyy}-${mm < 10 ? '0' + mm : mm}-${dd < 10 ? '0' + dd : dd}`;

const doctors = {
    Therapist: 'Терапевт',
    Cardiologist: 'Кардіолог',
    Dentist: 'Стоматолог',
};

const urgencyOptions = {
    usual: 'звичайна',
    priority: 'середня',
    high: 'термінова',
};

const statusOptions = {
    open: 'відкритий',
    closed: 'закритий'
};

export const visitBase = {
    class: 'Visit',
    fields: [
        {name: 'id', elementType: 'input', type: 'hidden', id: 'id', required: true},
        {name: 'dateVisit', label: 'Дата візиту', elementType: 'input', type: 'date', value: currDate, required: true, id: 'dateVisit', classNames: inputClasses.visitInputs},
        {name: 'goal', label: 'Мета візиту', elementType: 'input', type: 'text', required: true, id: 'visitTarget', classNames: inputClasses.visitInputs},
        {name: 'description', label: 'Деталі', elementType: 'textarea', type: 'text', id: 'shortDesc', classNames: inputClasses.visitInputs},
        {name: 'urgency', label: 'Терміновість', elementType: 'select', options: urgencyOptions, id: 'selectUrgency', classNames: inputClasses.visitInputs},
        {name: 'fullName', label: 'ПІБ', elementType: 'input', type: 'text', required: true, id: 'fullName', classNames: inputClasses.visitInputs}
    ]
};

export const visitOptions = new Map([
    ['Cardiologist', {
        class: 'CardiologistVisit',
        fields: [
            {name: 'pressure', label: 'Тиск', elementType: 'input', type: 'text', id: 'bloodPressure', classNames: inputClasses.visitInputs, required: true},
            {name: 'bodyMass', label: 'Індекс маси', elementType: 'input', type: 'number', id: 'massIndex', classNames: inputClasses.visitInputs, required: true, minValue: '0'},
            {name: 'illness', label: 'Перенесені серцеві захворювання', elementType: 'input', type: 'text', id: 'pastDiseases', classNames: inputClasses.visitInputs, required: true},
            {name: 'age', label: 'Вік', elementType: 'input', type: 'number', id: 'age', classNames: inputClasses.visitInputs, required: true, minValue: '0'}
        ]
    }],
    ['Dentist', {
        class: 'DentistVisit',
        fields: [
            {name: 'lastVisit', label: 'Дата останнього візиту', elementType: 'input', type: 'date', required: true, id: 'lastVisit', classNames: inputClasses.visitInputs}
        ]
    }],
    ['Therapist', {
        class: 'TherapistVisit',
        fields: [
            {name: 'age', label: 'Вік', elementType: 'input', type: 'number',  id: 'age', required: true, classNames: inputClasses.visitInputs, minValue: '0'}
        ]
    }]
]);

export const visitClasses = { Visit, CardiologistVisit, DentistVisit, TherapistVisit };
export {doctors, urgencyOptions, statusOptions};
