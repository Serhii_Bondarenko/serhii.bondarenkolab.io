const API = 'https://ajax.test-danit.com/api/v2';

const RESOURCES = {
    login: 'cards/login',
    cards: 'cards'
};

export {
    API,
    RESOURCES
}
